<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {	
    	
  //   	$em       	= $this->getDoctrine()->getEntityManager();
		// $visitantes	= $em->getRepository('AppBundle:Estadisticas');

		// $resultadoVisitantes = $visitantes->obtenerVisita();

		// // echo "<pre>";
		// // print_r($resultadoVisitantes);

		// foreach ($resultadoVisitantes as $value) {
		// 	$numVisitaAnterior = $value['visitas'];
		// }

		// print_r($numVisitaAnterior);

		// if ($numVisitaAnterior != "") {
		// 	print_r("Si es difente");
		// }else{
		// 	print_r("Si es igual");
		// 	$resultadoCuentaVisitantes = $visitantes->obtenerCuentaVisita();
		// }

		// exit;		
		
		

		// incrementar las visitas y guardar el nuevo valor en la BBDD
        // $this->getEntityManager()->getConnection()
             // ->insert('estadisticas', array('visitas' => ++$visitas));

        return $this->render('default/index.html.twig');
    }

    /**
     * [Forma pura php de realizar el contador]
     *
     * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
     * date 2017-09-08
     * @version [1.0]
     * @return  [type] [description]
     */
    public function contador(){
    	if(isset($_COOKIE['contador'])){
    		setcookie('contador', $_COOKIE['contador']+1, time()+365*24*60*60);
    		echo "Número de visitas:".$_COOKIE['contador'];
    	} else{
    		setcookie('contador',1,time()+365*24*60*60);
    		echo "Bienvenido por primera vez a nuestrea página";
    	}
    }
}
