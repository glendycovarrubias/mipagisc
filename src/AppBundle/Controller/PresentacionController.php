<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class PresentacionController extends Controller
{
	/*Para encontrar la ruta no olvidemos poner las anotaciones como es options y el name*/
    /**

     * @Route("/presentacion", options={"expose"=true}, name="presentacion")
     */
    public function presentacionAction(Request $request)
    {

        return $this->render('presentacion/presentacion.html.twig');
    }
}