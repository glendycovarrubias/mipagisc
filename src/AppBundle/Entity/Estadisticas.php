<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estadisticas
 *
 * @ORM\Table(name="estadisticas")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EstadisticasRepository")
 */
class Estadisticas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="visitas", type="integer")
     */
    private $visitas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visitas
     *
     * @param integer $visitas
     *
     * @return Estadisticas
     */
    public function setVisitas($visitas)
    {
        $this->visitas = $visitas;

        return $this;
    }

    /**
     * Get visitas
     *
     * @return int
     */
    public function getVisitas()
    {
        return $this->visitas;
    }
}

