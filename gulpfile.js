/**
 * Este archivo sirve para crear tarea y poder debuggiarlas, tambien nos
 * sirve para minificar archivos entre otras cosas, a esto se le conoce
 * como hacer tareas ESTO ES GULP 
 * Nota: Para poder usuar tu codigo en ES6 Necesitas instalar babel
 * ****** npm install babel-core babel-preset-env o  babel-preset-es2015 segun el caso
 * ****** gulp gulp-babel
 */

/*
 * Dependencias
 */
const 
  gulp      = require('gulp'),
  concat    = require('gulp-concat'),
  minifyJS  = require('gulp-uglify'),
  minifyCSS = require('gulp-minify-css'),
  plumber   = require('gulp-plumber'),
  babel     = require('gulp-babel');
  notify    = require('gulp-notify'),

/*
 * Configuración de la tarea 'demo'
 */
//Esta primera Tarea la tengo transformada en ES6
gulp.task('minify-js', () =>
  // return gulp.src('web/public/src/js/main.js')
  gulp.src('web/public/src/js/*.js')
    .pipe(babel({
        presets: ['env'] //Esta configuracion podria ponerse aparte .babelrc
    }))
    .pipe(concat('main.min.js'))
    .pipe(minifyJS())
    .on("error", errorAlertJS)
    .pipe(plumber())
    .pipe(gulp.dest('web/public/js'))
    .pipe(notify({
        message: 'JavaScript Complete'
    }))
);

//Esta segunda tarea la deje es5 pero podria cambiarlas
gulp.task('minify-css', function(){
  return gulp.src('web/public/src/css/*.css')
    .pipe(concat('main.min.css'))
    .on("error", errorAlertJS)
    .pipe(minifyCSS())
    .pipe(gulp.dest('web/public/css'))
    .pipe(notify({
        message: 'Css Complete'
    }));
});

function errorAlertJS(error){
    //Aqui configuramos el titulo y subtitulo del mensaje de error,
    notify.onError({
        title    : "Gulp JavaScript",
        subtitle : "Algo esta mal en tu javascript",
        sound    : "Basso"
    })(error);
    //También podemos pintar el error en el terminal
    console.log(error.toString());
    this.emit("end");
}

/* Tarea para estar debuggiando (copilando) el proyecto *ES6 function arrow ()=>* */
gulp.task('watch', () => {
    // gulp.watch('web/public/src/js/main.js', ['minify-js']);
    gulp.watch('web/public/src/js/*.js', ['minify-js']);
    gulp.watch('web/public/src/css/*.css', ['minify-css']);
});

/* Tarea para construir todas las tareas de gulp con solo poner gulp */
gulp.task('default',['minify-js', 'minify-css']);
